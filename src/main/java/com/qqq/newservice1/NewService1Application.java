package com.qqq.newservice1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewService1Application {

	public static void main(String[] args) {
		SpringApplication.run(NewService1Application.class, args);
	}

}
